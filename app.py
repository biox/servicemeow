from flask import Flask
import os
import pysnow
import requests

app = Flask(__name__, template_folder='templates')

def incident_query(query):
    s = requests.Session
    s.verify = False
    s.auth = requests.auth.HTTPBasicAuth(os.getenv('SNOW_USER'), os.getenv('SNOW_PASS'))
    c = pysnow.Client(instance=os.getenv('SNOW_ENDPOINT'), session=s)
    incident = c.resource(api_path='/table/incident')
    response = incident.get(query=query).all()

@app.route('/incidents')
def incidents():
    priority_qb = (
        pysnow.QueryBuilder()
        .field('assignment_group').equals('aisdjoqijwodjqwodi')
        .AND()
        .field('state').greater_than(0)
        .AND()
        .field('priority').between(1, 2)
    )

    other_qb = (
        pysnow.QueryBuilder()
        .field('assignment_group').equals('aisdjoqijwodjqwodi')
        .AND()
        .field('state').greater_than(0)
        .AND()
        .field('priority').between(3, 5)
    )

    return render_template('incidents.html', priority_payload=incident_query(priority_qb), other_payload=incident_query(other_qb))

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
